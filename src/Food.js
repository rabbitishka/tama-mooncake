class Food {
    power = 0;

    apply(tamagotchi) {
        tamagotchi.hungry = Math.max(0, tamagotchi.hungry - this.power);
    }
}

export class Apple extends Food {
    power = 5;
}

export class Potato extends Food {
    power = 10;
}

export class Watermelon extends Food {
    toiletImpact = 10;
    power = 1;

    apply(tamagotchi) {
        super.apply(tamagotchi);
        tamagotchi.toilet = Math.min(100, tamagotchi.toilet + this.toiletImpact);
    }

}

