import {Tamagotchi} from "./Tamagotchi";
import {Apple, Potato, Watermelon} from "./Food";
import * as PIXI from '../node_modules/pixi.js';
import MookcakeImage from "./images/mooncake.png";
import MooncakeFed from "./images/mooncake_fed-1.png";
import MooncakeDead from "./images/dead-mooncake-1.png";
import {createButton} from "./Button";
// import {Poop} from "./Toilet";


const instance = new Tamagotchi();

const app = new PIXI.Application({ width: 640, height: 500, background: '#B6D8F2' });
document.body.appendChild(app.view);

// Создание персонажа
const characterSprite = PIXI.Sprite.from(MookcakeImage);
const character = app.stage.addChild(characterSprite);

// Сытый персонаж
// const caracterSpriteFed = PIXI.Sprite.from(MooncakeFed);
// const caracterFed = app.stage.addChild(caracterSpriteFed);
// caracterFed.visible = !caracterFed.visible;

// Здоровье
const healthText = new PIXI.Text('Health: 100%');
healthText.x = 350;
healthText.y = 50;

// Состояние
const propsText = new PIXI.Text('State');
propsText.x = 350;
propsText.y = 100;

// Добавляем на экран
app.stage.addChild(healthText);
app.stage.addChild(propsText);

// Кнопка для сна
const sleepButton = createButton("Sleep", () => {
    instance.sleep();
});
sleepButton.x = 350;
sleepButton.y = 250;

app.stage.addChild(sleepButton);

// Кнопка для поедания яблока
const eatAppleButton = createButton("Eat Apple", () => {
    const apple = new Apple();
    apple.apply(instance);
});
eatAppleButton.x = 350;
eatAppleButton.y = 290;

app.stage.addChild(eatAppleButton);

// Кнопка для поедания картошки
const eatPotatoButton = createButton("Eat Potato", () => {
    const potato = new Potato();
    potato.apply(instance);
});
eatPotatoButton.x = 350;
eatPotatoButton.y = 330;

app.stage.addChild(eatPotatoButton);

// Кнопка для поедания арбуза
const eatWmelonButton = createButton("Eat Wmelon", () => {
    const watermelon = new Watermelon();
    watermelon.apply(instance);
    // character.visible = !character.visible;
    // app.stage.removeChild(character);
    // const caracterSpriteFed = PIXI.Sprite.from(MooncakeFed);
    // const caracterFed = app.stage.addChild(caracterSpriteFed);
    app.stage.addChild(caracterFed);
    // caracterFed.visible = !caracterFed.visible;
    // app.stage.removeChild(caracterFed);
    // setTimeout(() => {
    //     // app.stage.addChild(caracterFed);
    //     app.stage.removeChild(caracterFed);
    //     // character = app.stage.addChild(character);
    // }, 1000);
});
eatWmelonButton.x = 350;
eatWmelonButton.y = 370;

app.stage.addChild(eatWmelonButton);

// let originalSprite = characterSprite;
// const eatWmelonButton = createButton("Eat Wmelon", () => {
//     const watermelon = new Watermelon();
//     watermelon.apply(instance);
//     app.stage.removeChild(character);
//     const fedSprite = PIXI.Sprite.from(MooncakeFed);
//     character = app.stage.addChild(fedSprite);
//     setTimeout(() => {
//         app.stage.removeChild(character);
//         character = app.stage.addChild(originalSprite);
//     }, 1000);
// });


// Кнопка для посрат
const cacaButton = createButton("Poop", () => {
    // const caca = new Poop();
    // caca.apply(instance);
    instance.sheeeeeeit();
});
cacaButton.x = 350;
cacaButton.y = 410;

app.stage.addChild(cacaButton);

// Кнопка для играт
const playButton = createButton("Play", () => {
    instance.play();
});
playButton.x = 350;
playButton.y = 450;

app.stage.addChild(playButton);

// Игровой цикл
app.ticker.add((delta) => {

    if (!instance.checkIsDead()) {
        instance.tick(delta)
    } else {
        app.stage.removeChild(character)
        const caracterSpriteDead = PIXI.Sprite.from(MooncakeDead);
        const caracterDead = app.stage.addChild(caracterSpriteDead);
        app.stage.addChild(caracterDead);
    }

    healthText.text = `Health: ${instance.health.toFixed(0)}%`

    propsText.text =  `Hungry: ${instance.hungry.toFixed(0)}%\r` +
        `Sleep: ${instance.energy.toFixed(0)}%\r` +
        `Toilet: ${instance.toilet.toFixed(0)}%\r` +
        `Happiness: ${instance.happiness.toFixed(0)}%\r`;
});


