

const propertyMultiplier = 0.1;

const TamagotchiState = {
    Idle: 1,
    Sleeping: 2,
    Eating: 4,
    Playing: 8,
    Shitting: 16
}

export class Tamagotchi {

    age = 0;
    health = 100; // 0 - 100

    happiness = 100; // 0 - 100
    hungry = 0; // 0 - 100
    energy = 100; // 0 - 100
    toilet = 0;

    // Текущее состояние
    _currentState = TamagotchiState.Idle;

    // Выполняется каждый кадр
    tick(deltaTime) {

        switch (this._currentState) {

            // Тамагочи ничего не делает
            case TamagotchiState.Idle:
                this.toilet += propertyMultiplier * deltaTime;
                this.energy -= propertyMultiplier * deltaTime;
                this.hungry += propertyMultiplier * deltaTime;
                this.happiness -= propertyMultiplier * deltaTime;
                break;

            // Тамагочи серил
            case TamagotchiState.Shitting:
                this.toilet -= 0.05 * deltaTime;
                if (this.toilet <= 0) {
                    this.toilet = 0;
                    this.setState(TamagotchiState.Idle);
                }
                break;

            case TamagotchiState.Sleeping:
                this.energy += 0.05 * deltaTime;
                if (this.energy >= 100) {
                    this.energy = 100;
                    this.setState(TamagotchiState.Idle);
                }
                break;

            case TamagotchiState.Eating:
                this.hungry -= 0.05 * deltaTime;
                if (this.hungry <= 0) {
                    this.hungry = 0;
                    this.setState(TamagotchiState.Idle);
                }
                break;
        }

        // Обновляем здоровье
        this.calculateHealth();

        // Если мертв, не запрашиваем следующий раз кадр
        if (this.checkIsDead()) {
            console.log("Oh no I'm dead")
        }
    }

    calculateHealth() {
        this.health = Math.min(this.energy, this.happiness, 100 - this.toilet, 100 - this.hungry);
    }

    checkIsDead() {
        return this.health <= 0 || this.age >= 100;
    }

    // Смена состояния
    setState(newState) {
        this._currentState = newState;
    }

    feed(food) {
        this.hungry = Math.max(0, this.hungry - food.power);
        // this.setState(TamagotchiState.Eating);
    }

    sheeeeeeit() {
        this.setState(TamagotchiState.Shitting);
    }

    sleep() {
        this.setState(TamagotchiState.Sleeping);
    }

    play() {
        this.happiness = Math.min(100, this.happiness + 5);
    }
}
