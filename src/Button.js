import ButtonImage from "./images/button.png"
import * as PIXI from 'pixi.js';


export const createButton = (text, onClick) => {

    const sprite = PIXI.Sprite.from(ButtonImage);
    sprite.on('pointerdown', onClick);

    const caption = new PIXI.Text(text);
    caption.x = 10;

    sprite.addChild(caption)

    sprite.interactive = true;
    sprite.buttonMode = true;

    return sprite;
}
